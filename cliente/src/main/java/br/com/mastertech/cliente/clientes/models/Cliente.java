package br.com.mastertech.cliente.clientes.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    @NotNull(message = "O nome é obrigatório")
    @Size(min = 3, message = "Nome tem que ter no minimo 3 caracteres")
    private String nome;

    public Cliente (){}

    public Cliente(int id, @NotNull(message = "O nome é obrigatório") @Size(min = 3, message = "Nome tem que ter no minimo 3 caracteres") String nome) {
        this.id = id;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
