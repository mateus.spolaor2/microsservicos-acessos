package br.com.mastertech.cliente.clientes.services;

import br.com.mastertech.cliente.clientes.DTOs.InsereClienteDTO;
import br.com.mastertech.cliente.clientes.exceptions.ClienteNotFoundException;
import br.com.mastertech.cliente.clientes.models.Cliente;
import br.com.mastertech.cliente.clientes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public Cliente cadastrarCliente(InsereClienteDTO clienteDTO) {

        Cliente cliente = new Cliente();
        cliente.setNome(clienteDTO.getNome());

        return clienteRepository.save(cliente);
    }

    public Cliente findClientById(int id) {

        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent()) {
            return clienteOptional.get();
        }

        throw new ClienteNotFoundException();
    }
}
