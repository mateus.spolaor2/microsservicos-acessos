package br.com.mastertech.acesso.acessos.producers;

import br.com.mastertech.acesso.acessos.DTOs.AcessoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducer {

    @Autowired
    private KafkaTemplate<String, AcessoDTO> producer;

    public void enviarAcessoKafka(AcessoDTO acesso) {
        producer.send("spec4-mateus-affonso", acesso);
    }

}
