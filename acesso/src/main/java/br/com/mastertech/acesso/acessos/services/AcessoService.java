package br.com.mastertech.acesso.acessos.services;

import br.com.mastertech.acesso.acessos.DTOs.AcessoDTO;
import br.com.mastertech.acesso.acessos.clients.ClienteClient;
import br.com.mastertech.acesso.acessos.clients.PortaClient;
import br.com.mastertech.acesso.acessos.clients.exceptions.ClienteNotFoundException;
import br.com.mastertech.acesso.acessos.clients.exceptions.PortaNotFoundException;
import br.com.mastertech.acesso.acessos.models.Acesso;
import br.com.mastertech.acesso.acessos.models.Cliente;
import br.com.mastertech.acesso.acessos.models.Porta;
import br.com.mastertech.acesso.acessos.repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    ClienteClient clienteClient;

    @Autowired
    PortaClient portaClient;

    @Autowired
    AcessoRepository acessoRepository;

    public AcessoDTO cadastrarAcesso(AcessoDTO acessoDTO) {

        Acesso novoAcesso = verificaIntegridadeDeAcesso(acessoDTO.getClienteId(), acessoDTO.getPortaId());

        if(novoAcesso == null){
            acessoRepository.save(novoAcesso);
        } else {
            throw new RuntimeException("Acesso já concedido para este cliente nesta porta");
        }

        return acessoDTO;
    }

    public void deletaAcesso(int clienteId, int portaId) {

        Acesso deletarAcesso = verificaIntegridadeDeAcesso(clienteId, portaId);

        if(deletarAcesso == null){
            throw new RuntimeException("Não existe acessos para esse cliente nesta porta");
        } else {
            acessoRepository.deleteById(deletarAcesso.getId());
        }

    }

    private Acesso verificaIntegridadeDeAcesso(int clienteId, int portaId){
        Optional<Porta> portaOptional = portaClient.getPorta(portaId);

        if(portaOptional.isPresent()){

            Optional<Cliente> clienteOptional = clienteClient.getCliente(clienteId);

            if(clienteOptional.isPresent()){

                Optional<Acesso> acessoOptional = acessoRepository.findByPortaIdAndClienteId(portaId, clienteId);

                if(acessoOptional.isPresent()) {
                    return acessoOptional.get();
                } else {
                    return null;
                }
            } else {
                throw new ClienteNotFoundException();
            }
        } else {
            throw new PortaNotFoundException();
        }
    }
}
