package br.com.mastertech.acesso.acessos.DTOs;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AcessoDTO {

    @JsonProperty("porta_id")
    private int portaId;

    @JsonProperty("cliente_id")
    private int clienteId;

    public AcessoDTO(){}

    public AcessoDTO(int portaId, int clienteId) {
        this.portaId = portaId;
        this.clienteId = clienteId;
    }

    public int getPortaId() {
        return portaId;
    }

    public void setPortaId(int portaId) {
        this.portaId = portaId;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}
