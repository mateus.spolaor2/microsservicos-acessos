package br.com.mastertech.acesso.acessos.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Acesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonProperty("porta_id")
    private int portaId;

    @JsonProperty("cliente_id")
    private int clienteId;

    public Acesso(){}

    public Acesso(int portaId, int clienteId) {
        this.portaId = portaId;
        this.clienteId = clienteId;
    }

    public Acesso(int id, int portaId, int clienteId) {
        this.id = id;
        this.portaId = portaId;
        this.clienteId = clienteId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPortaId() {
        return portaId;
    }

    public void setPortaId(int portaId) {
        this.portaId = portaId;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}
