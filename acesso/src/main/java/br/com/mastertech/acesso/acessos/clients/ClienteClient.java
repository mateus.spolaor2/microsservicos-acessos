package br.com.mastertech.acesso.acessos.clients;

import br.com.mastertech.acesso.acessos.clients.decoder.cliente.ClienteClientConfiguration;
import br.com.mastertech.acesso.acessos.models.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {
    @GetMapping("/cliente/{id}")
    Optional<Cliente> getCliente(@PathVariable int id);
}
