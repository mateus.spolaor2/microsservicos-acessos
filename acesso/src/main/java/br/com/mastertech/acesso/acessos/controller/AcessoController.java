package br.com.mastertech.acesso.acessos.controller;

import br.com.mastertech.acesso.acessos.DTOs.AcessoDTO;
import br.com.mastertech.acesso.acessos.producers.AcessoProducer;
import br.com.mastertech.acesso.acessos.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    AcessoService acessoService;

    @Autowired
    AcessoProducer acessoProducer;
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AcessoDTO cadastraAcesso(@RequestBody AcessoDTO acessoDTO) {
        AcessoDTO acesso = acessoService.cadastrarAcesso(acessoDTO);
        //AcessoDTO acesso = new AcessoDTO(2,3);
        //acessoProducer.enviarAcessoKafka(acesso);
        
        return acesso;
    }

    @DeleteMapping("/{clienteId}/{portaId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletaAcesso(
            @PathVariable(name = "clienteId") int clienteId, @PathVariable(name = "portaId") int portaId ){

        acessoService.deletaAcesso(clienteId, portaId);

    }

}
