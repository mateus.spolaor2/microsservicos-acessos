package br.com.kafka.acessoconsumer.consumer.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Acesso {

    @JsonProperty("porta_id")
    private int portaId;

    @JsonProperty("cliente_id")
    private int clienteId;

    public Acesso(){}

    public Acesso(int portaId, int clienteId) {
        this.portaId = portaId;
        this.clienteId = clienteId;
    }

    public int getPortaId() {
        return portaId;
    }

    public void setPortaId(int portaId) {
        this.portaId = portaId;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

}
