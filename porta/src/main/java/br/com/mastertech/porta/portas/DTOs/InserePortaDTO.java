package br.com.mastertech.porta.portas.DTOs;

public class InserePortaDTO {

    private String sala;

    private String andar;

    public InserePortaDTO(){}

    public InserePortaDTO(String sala, String andar) {
        this.sala = sala;
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }
}
